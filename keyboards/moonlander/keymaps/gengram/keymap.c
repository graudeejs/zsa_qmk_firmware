#include QMK_KEYBOARD_H
#include "version.h"
#define MOON_LED_LEVEL LED_LEVEL

enum custom_keycodes {
  RGB_SLD = ML_SAFE_RANGE,
};


const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {
  [0] = LAYOUT_moonlander(
    KC_LBRC,        KC_1,           KC_2,           KC_3,           KC_4,           KC_5,           LCTL(KC_A),                                     TO(3),          KC_6,           KC_7,           KC_8,           KC_9,           KC_0,           KC_RBRC,
    KC_GRAVE,       KC_B,           KC_Y,           KC_O,           KC_U,           KC_QUOTE,       LCTL(KC_C),                                     OSL(2),         KC_SCLN,        KC_L,           KC_D,           KC_W,           KC_V,           KC_COLN,
    MT(MOD_LSFT, KC_Z),MT(MOD_LGUI, KC_C),MT(MOD_LALT, KC_I),MT(MOD_LCTL, KC_E),MT(MOD_RALT, KC_A),LT(1,KC_COMMA), LCTL(KC_V),                                                                     OSL(1),         LT(1,KC_DOT),   MT(MOD_RALT, KC_H),MT(MOD_LCTL, KC_T),MT(MOD_LALT, KC_S),MT(MOD_LGUI, KC_N),MT(MOD_LSFT, KC_Q),
    KC_BSLS,        KC_G,           KC_X,           KC_J,           KC_K,           KC_MINUS,                                       KC_SLASH,       KC_R,           KC_M,           KC_F,           KC_P,           KC_EQUAL,
    LCTL(KC_TAB),   KC_LEFT,        KC_DOWN,        KC_UP,          KC_RIGHT,       CW_TOGG,                                                                                                        RGB_TOG,        KC_H,           KC_J,           KC_K,           KC_L,           KC_ESCAPE,
    MT(MOD_LSFT, KC_ENTER),LT(2,KC_TAB),   KC_BSPC,                        OSM(MOD_LCTL),  KC_BSPC,        MT(MOD_RSFT, KC_SPACE)
  ),
  [1] = LAYOUT_moonlander(
    KC_TILD,        KC_GRAVE,       KC_LPRN,        KC_RPRN,        KC_SCLN,        KC_COMMA,       KC_TRANSPARENT,                                 KC_TRANSPARENT, KC_CIRC,        KC_7,           KC_8,           KC_9,           KC_EXLM,        KC_DLR,
    KC_EXLM,        KC_LCBR,        KC_QUOTE,       KC_DQUO,        KC_RCBR,        KC_QUES,        KC_TRANSPARENT,                                 KC_TRANSPARENT, KC_PERC,        KC_4,           KC_5,           KC_6,           KC_EQUAL,       KC_COLN,
    KC_HASH,        KC_CIRC,        KC_EQUAL,       KC_UNDS,        KC_DLR,         KC_ASTR,        KC_TRANSPARENT,                                                                 TO(0),          KC_PLUS,        KC_1,           KC_2,           KC_3,           KC_MINUS,       KC_ENTER,
    KC_BSLS,        KC_LABK,        KC_PIPE,        KC_MINUS,       KC_RABK,        KC_SLASH,                                       KC_ASTR,        KC_0,           KC_DOT,         KC_COMMA,       KC_SLASH,       KC_ENTER,
    KC_AT,          KC_AMPR,        KC_LBRC,        KC_RBRC,        KC_PLUS,        KC_CAPS,                                                                                                        KC_TRANSPARENT, KC_LEFT,        KC_DOWN,        KC_UP,          KC_RIGHT,       KC_ESCAPE,
    KC_TRANSPARENT, KC_TRANSPARENT, KC_BSPC,                        KC_TRANSPARENT, KC_BSPC,        KC_TAB
  ),
  [2] = LAYOUT_moonlander(
    KC_SCRL,        KC_F1,          KC_F2,          KC_F3,          KC_F4,          KC_F5,          KC_NO,                                          KC_TRANSPARENT, KC_F6,          KC_F7,          KC_F8,          KC_F9,          KC_F10,         KC_F11,
    KC_PSCR,        KC_MS_WH_UP,    KC_MS_BTN1,     KC_MS_UP,       KC_MS_BTN2,     KC_NO,          KC_NO,                                          TO(0),          KC_NO,          KC_NO,          KC_NO,          KC_NO,          KC_NO,          KC_F12,
    KC_NO,          KC_MS_WH_DOWN,  KC_MS_LEFT,     KC_MS_DOWN,     KC_MS_RIGHT,    KC_NO,          KC_NO,                                                                          KC_TRANSPARENT, KC_NO,          KC_NO,          KC_NO,          KC_NO,          KC_NO,          KC_F13,
    KC_NO,          KC_NO,          KC_MS_WH_LEFT,  KC_MS_BTN3,     KC_MS_WH_RIGHT, KC_NO,                                          KC_AUDIO_MUTE,  KC_MEDIA_PREV_TRACK,KC_AUDIO_VOL_DOWN,KC_AUDIO_VOL_UP,KC_MEDIA_NEXT_TRACK,KC_PAGE_UP,
    KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, AU_TOGG,                                                                                                        MU_TOGG,        KC_LEFT,        KC_DOWN,        KC_UP,          KC_RIGHT,       KC_PGDN,
    KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT,                 KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT
  ),
  [3] = LAYOUT_moonlander(
    KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT,                                 TO(0),          KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT,
    KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT,                                 KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT,
    KC_Z,           KC_C,           KC_I,           KC_E,           KC_A,           KC_TRANSPARENT, KC_TRANSPARENT,                                                                 KC_TRANSPARENT, KC_TRANSPARENT, KC_H,           KC_T,           KC_S,           KC_N,           KC_Q,
    KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT,                                 KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT,
    KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT,                                                                                                 KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT,
    KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT,                 KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT
  ),
};
const uint16_t PROGMEM combo0[] = { KC_LBRC, KC_RBRC, COMBO_END};

combo_t key_combos[COMBO_COUNT] = {
    COMBO(combo0, QK_MAGIC_TOGGLE_CTL_GUI),
};
uint16_t get_tapping_term(uint16_t keycode, keyrecord_t *record) {
    switch (keycode) {
        case MT(MOD_LSFT, KC_Z):
            return TAPPING_TERM + 50;
        case MT(MOD_LGUI, KC_C):
            return TAPPING_TERM + 50;
        case MT(MOD_LALT, KC_I):
            return TAPPING_TERM + 30;
        case MT(MOD_LALT, KC_S):
            return TAPPING_TERM + 30;
        case MT(MOD_LGUI, KC_N):
            return TAPPING_TERM + 50;
        case MT(MOD_LSFT, KC_Q):
            return TAPPING_TERM + 50;
        default:
            return TAPPING_TERM;
    }
}

extern rgb_config_t rgb_matrix_config;

void keyboard_post_init_user(void) {
  rgb_matrix_enable();
}


const uint8_t PROGMEM ledmap[][RGB_MATRIX_LED_COUNT][3] = {
    [0] = { {129,255,255}, {129,255,255}, {129,255,255}, {129,255,255}, {129,255,255}, {129,255,255}, {129,255,255}, {129,255,255}, {129,255,255}, {0,255,255}, {129,255,255}, {129,255,255}, {129,255,255}, {129,255,255}, {0,255,255}, {129,255,255}, {129,255,255}, {129,255,255}, {129,255,255}, {0,255,255}, {129,255,255}, {129,255,255}, {129,255,255}, {129,255,255}, {0,255,255}, {129,255,255}, {129,255,255}, {129,255,255}, {129,255,255}, {0,255,255}, {0,255,255}, {0,255,255}, {129,255,255}, {129,255,255}, {129,255,255}, {129,255,255}, {129,255,255}, {129,255,255}, {129,255,255}, {129,255,255}, {129,255,255}, {129,255,255}, {129,255,255}, {129,255,255}, {129,255,255}, {0,255,255}, {129,255,255}, {129,255,255}, {129,255,255}, {129,255,255}, {0,255,255}, {129,255,255}, {129,255,255}, {129,255,255}, {129,255,255}, {0,255,255}, {129,255,255}, {129,255,255}, {129,255,255}, {129,255,255}, {0,255,255}, {129,255,255}, {129,255,255}, {129,255,255}, {129,255,255}, {86,255,255}, {179,255,194}, {215,255,255}, {129,255,255}, {129,255,255}, {129,255,255}, {129,255,255} },

    [1] = { {215,255,255}, {215,255,255}, {215,255,255}, {215,255,255}, {215,255,255}, {215,255,255}, {215,255,255}, {215,255,255}, {215,255,255}, {215,255,255}, {215,255,255}, {215,255,255}, {215,255,255}, {215,255,255}, {215,255,255}, {215,255,255}, {215,255,255}, {215,255,255}, {215,255,255}, {215,255,255}, {215,255,255}, {215,255,255}, {215,255,255}, {215,255,255}, {215,255,255}, {215,255,255}, {215,255,255}, {215,255,255}, {215,255,255}, {215,255,255}, {215,255,255}, {215,255,255}, {215,255,255}, {215,255,255}, {215,255,255}, {215,255,255}, {215,255,255}, {215,255,255}, {215,255,255}, {215,255,255}, {215,255,255}, {215,255,255}, {215,255,255}, {215,255,255}, {215,255,255}, {215,255,255}, {215,255,255}, {215,255,255}, {215,255,255}, {215,255,255}, {215,255,255}, {215,255,255}, {215,255,255}, {215,255,255}, {215,255,255}, {215,255,255}, {215,255,255}, {215,255,255}, {215,255,255}, {215,255,255}, {215,255,255}, {215,255,255}, {215,255,255}, {215,255,255}, {215,255,255}, {86,255,255}, {179,255,194}, {129,255,255}, {215,255,255}, {215,255,255}, {215,255,255}, {215,255,255} },

    [2] = { {179,255,194}, {179,255,194}, {179,255,194}, {179,255,194}, {179,255,194}, {179,255,194}, {179,255,194}, {179,255,194}, {179,255,194}, {0,255,255}, {179,255,194}, {179,255,194}, {179,255,194}, {179,255,194}, {0,255,255}, {179,255,194}, {179,255,194}, {179,255,194}, {179,255,194}, {0,255,255}, {179,255,194}, {179,255,194}, {179,255,194}, {179,255,194}, {0,255,255}, {179,255,194}, {179,255,194}, {179,255,194}, {179,255,194}, {179,255,194}, {179,255,194}, {179,255,194}, {179,255,194}, {179,255,194}, {179,255,194}, {179,255,194}, {179,255,194}, {179,255,194}, {179,255,194}, {179,255,194}, {179,255,194}, {179,255,194}, {179,255,194}, {179,255,194}, {179,255,194}, {179,255,194}, {179,255,194}, {179,255,194}, {179,255,194}, {179,255,194}, {179,255,194}, {179,255,194}, {179,255,194}, {179,255,194}, {179,255,194}, {179,255,194}, {179,255,194}, {179,255,194}, {179,255,194}, {179,255,194}, {179,255,194}, {179,255,194}, {179,255,194}, {179,255,194}, {179,255,194}, {86,255,255}, {129,255,255}, {215,255,255}, {179,255,194}, {179,255,194}, {179,255,194}, {179,255,194} },

    [3] = { {86,255,255}, {86,255,255}, {86,255,255}, {86,255,255}, {86,255,255}, {86,255,255}, {86,255,255}, {86,255,255}, {86,255,255}, {0,255,255}, {86,255,255}, {86,255,255}, {86,255,255}, {86,255,255}, {0,255,255}, {86,255,255}, {86,255,255}, {86,255,255}, {86,255,255}, {0,255,255}, {86,255,255}, {86,255,255}, {86,255,255}, {86,255,255}, {0,255,255}, {86,255,255}, {86,255,255}, {86,255,255}, {86,255,255}, {0,255,255}, {0,255,255}, {0,255,255}, {86,255,255}, {86,255,255}, {86,255,255}, {86,255,255}, {86,255,255}, {86,255,255}, {86,255,255}, {86,255,255}, {86,255,255}, {86,255,255}, {86,255,255}, {86,255,255}, {86,255,255}, {0,255,255}, {86,255,255}, {86,255,255}, {86,255,255}, {86,255,255}, {0,255,255}, {86,255,255}, {86,255,255}, {86,255,255}, {86,255,255}, {0,255,255}, {86,255,255}, {86,255,255}, {86,255,255}, {86,255,255}, {0,255,255}, {86,255,255}, {86,255,255}, {86,255,255}, {86,255,255}, {129,255,255}, {179,255,194}, {215,255,255}, {86,255,255}, {86,255,255}, {86,255,255}, {86,255,255} },

};

void set_layer_color(int layer) {
  for (int i = 0; i < RGB_MATRIX_LED_COUNT; i++) {
    HSV hsv = {
      .h = pgm_read_byte(&ledmap[layer][i][0]),
      .s = pgm_read_byte(&ledmap[layer][i][1]),
      .v = pgm_read_byte(&ledmap[layer][i][2]),
    };
    if (!hsv.h && !hsv.s && !hsv.v) {
        rgb_matrix_set_color( i, 0, 0, 0 );
    } else {
        RGB rgb = hsv_to_rgb( hsv );
        float f = (float)rgb_matrix_config.hsv.v / UINT8_MAX;
        rgb_matrix_set_color( i, f * rgb.r, f * rgb.g, f * rgb.b );
    }
  }
}

bool rgb_matrix_indicators_user(void) {
  if (rawhid_state.rgb_control) {
      return false;
  }
  if (keyboard_config.disable_layer_led) { return false; }
  switch (biton32(layer_state)) {
    case 0:
      set_layer_color(0);
      break;
    case 1:
      set_layer_color(1);
      break;
    case 2:
      set_layer_color(2);
      break;
    case 3:
      set_layer_color(3);
      break;
   default:
    if (rgb_matrix_get_flags() == LED_FLAG_NONE)
      rgb_matrix_set_color_all(0, 0, 0);
    break;
  }
  return true;
}

bool process_record_user(uint16_t keycode, keyrecord_t *record) {
  switch (keycode) {

    case RGB_SLD:
        if (rawhid_state.rgb_control) {
            return false;
        }
        if (record->event.pressed) {
            rgblight_mode(1);
        }
        return false;
  }
  return true;
}



